package fs

import (
	"bytes"
	"fmt"
	"log"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

// Uploads a file to an S3 bucket via the os.File struct given the bucket name,
// region and filename (full key including the "path")
func Upload(r *bytes.Buffer, bucketName, region, filename string, wg *sync.WaitGroup) s3manager.UploadOutput {
	fmt.Println("Uploading")
	//defer wg.Done()
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(region),
	}))

	uploader := s3manager.NewUploader(sess)

	res, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(filename),
		Body:   r,
	})
	HandleError(err)

	return *res
}

// Downloads a file from S3 given a bucket name, region and filename (full key including "path")
func Download(bucketName, region, filename string, wg *sync.WaitGroup) *bytes.Buffer {
	fmt.Println("downloading")
	//defer wg.Done()
	sess := session.Must(session.NewSession(&aws.Config{
		Region: aws.String(region),
	}))

	downloader := s3manager.NewDownloader(sess)
	writer := aws.NewWriteAtBuffer([]byte{})

	Object := &s3.GetObjectInput{
		Bucket: aws.String(bucketName),
		Key:    aws.String(filename),
	}

	n, err := downloader.Download(writer, Object)
	HandleError(err)

	fmt.Println(fmt.Sprintf("Number of bytes downloaded: %d", n))

	return bytes.NewBuffer(writer.Bytes())
}

func HandleError(err error) {
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
}
