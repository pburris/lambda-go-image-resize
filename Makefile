build:
	dep ensure
	env GOOS=linux go build -ldflags="-s -w" -o bin/resize cmd/resize.go

deploy:
	make build
	sls deploy
