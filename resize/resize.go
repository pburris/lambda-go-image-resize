package resize

import (
	"bytes"
	"fmt"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"sync"

	"golang.org/x/image/draw"
)

func Image(input *bytes.Buffer, width int, ext string, wg *sync.WaitGroup) *bytes.Buffer {
	switch ext {
	case ".png":
		return rpng(input, width, wg)
	case ".gif":
		return rgif(input, width, wg)
	case ".jpeg":
		return rjpeg(input, width, wg)
	default:
		return input
	}
}

func Resize(input image.Image, width int, wg *sync.WaitGroup) image.Image {
	//defer wg.Done()
	fmt.Println("Resizing")
	width, height := GetWidthHeight(width, input.Bounds().Max.X, input.Bounds().Max.Y)
	ret := image.NewRGBA(image.Rect(0, 0, width, height))
	draw.ApproxBiLinear.Scale(ret, ret.Bounds(), input, input.Bounds(), draw.Src, nil)
	return ret
}

func rpng(input *bytes.Buffer, width int, wg *sync.WaitGroup) *bytes.Buffer {
	//wg.Add(1)
	//defer wg.Done()
	img, err := png.Decode(input)
	HandleError(err)

	resized := Resize(img, width, wg)
	buf := new(bytes.Buffer)
	err = png.Encode(buf, resized)
	HandleError(err)

	return buf
}

func rjpeg(input *bytes.Buffer, width int, wg *sync.WaitGroup) *bytes.Buffer {
	//wg.Add(1)
	//defer wg.Done()
	img, err := jpeg.Decode(input)
	HandleError(err)

	resized := Resize(img, width, wg)
	buf := new(bytes.Buffer)
	err = jpeg.Encode(buf, resized, &jpeg.Options{Quality: 80})
	HandleError(err)

	return buf
}

func rgif(input *bytes.Buffer, width int, wg *sync.WaitGroup) *bytes.Buffer {
	//wg.Add(1)
	//defer wg.Done()
	img, err := gif.Decode(input)
	HandleError(err)

	resized := Resize(img, width, wg)
	buf := new(bytes.Buffer)
	err = gif.Encode(buf, resized, &gif.Options{})
	HandleError(err)

	return buf
}
