package resize

import (
	"fmt"
	"log"
)

func GetWidthHeight(newW int, oldW int, oldH int) (int, int) {
	newH := (oldH * newW) / oldW
	var width int
	var height int

	if newW > oldW {
		width = oldW
		height = oldH
	} else {
		width = newW
		height = newH
	}
	return width, height
}

func HandleError(err error) {
	if err != nil {
		fmt.Println(err)
		log.Fatal(err)
	}
}
