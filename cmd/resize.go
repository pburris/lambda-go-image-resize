package main

import (
	"bytes"
	"context"
	"fmt"
	"strings"
	"sync"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/jumballaya/image-resize/fs"
	"github.com/jumballaya/image-resize/resize"
)

var SIZES = map[string]int{
	"sm": 300,
	"md": 728,
	"lg": 960,
}

var wg sync.WaitGroup

func Handler(ctx context.Context, event events.S3Event) {
	fmt.Println("Starting...")
	for _, record := range event.Records {
		fmt.Println("Record")
		//wg.Add(1)
		s3 := record.S3

		bucket := s3.Bucket.Name
		region := "us-east-1"
		filename := s3.Object.Key

		// wg
		file := fs.Download(bucket, region, filename, &wg)

		files := make(map[string]*bytes.Buffer)
		for tag, w := range SIZES {
			fmt.Println("Making Image")
			//wg.Add(1)
			// Get path info
			pathSplit := strings.Split(filename, "/")
			path := strings.Join(pathSplit[:len(pathSplit)-1], "/")
			path = strings.Replace(path, "original", "resize", 1)
			name := strings.Join(pathSplit[len(pathSplit)-1:], "")
			newFilename := fmt.Sprintf("%s/%s-%s", path, tag, name)
			ext := strings.Split(name, ".")[1]

			// wg
			img := resize.Image(file, w, ext, &wg)

			fmt.Println(img)

			files[newFilename] = img
		}

		fmt.Println("About to Upload")
		for key, file := range files {
			//wg.Add(1)

			// wg
			fs.Upload(file, bucket, region, key, &wg)
		}
	}
}

func main() {
	lambda.Start(Handler)
}
